---
title: "RSA"
draft: false
date: "2021-12-17"
---

# Origin

RSA (Rivest–Shamir–Adleman) is a public-key cryptosystem that is widely used for secure data transmission. It is also one of the oldest.

# Kata

In this kata we will encrypt and decrypt a message with RSA algorythm.

## Build keys

* Find 2 primes numbers `p` and `q` such that `(2^24 + 1) < p × q < 2^32`
* Compute `N = p × q`
* Compute `n = (p − 1) × (q − 1)`
* Choose `c` a coprime number with `n` such that `1 < c < n`

`(N, c)` is the public key

* Determine `d` as `d ≡ c^−1 (mod n)` that is, `d` is the modular multiplicative inverse of `c modulo n`. `d` can be computed efficiently by using the Extended Euclidean algorithm.

`(N, d)` is the private key

## Encrypt the message

Make a function they:

* take a message in input
* slice the message in peaces of exactly 4 bytes
* for each peaces as integer `a` compute `a^c mod N`
* the concatenation of each of this numbers is the encrypted message

## Decrypt the message

Make a function they:

* take an encrypted message in input
* slice the message in peaces of exactly 4 bytes
* for each peaces as integer `a` compute `a^d mod N`
* the concatenation of each of this numbers is the original message

## Transmit the message

For transmition using mail or other messaging tools, we must have a function they encode the encrypted message in a readable message.

Make a function they take an encrypted message in input and they output a string with only readable chars like base64 encode. Do the revert function.


